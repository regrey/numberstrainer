package com.example.numberstrainer;

import org.junit.Before;
import org.junit.Test;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExamplePOJOUnitTest {

    private ExampleGenerator exampleGenerator;

    @Before
    public void init() {
        exampleGenerator = new ExampleGenerator();
    }

    @Test
    public void testGenerationPlusExample() {
        for (int i = 0; i < 10000000; i+=1) {
            Example example = exampleGenerator.generatePlusExample();
            assert example.getOperation().equals("+");
            assert example.getFirstElement() >= 0;
            assert example.getFirstElement() < 100;
            assert example.getSecondElement() >= 0;
            assert example.getSecondElement() < 100;
            assert example.getResult() >= 0;
            assert example.getResult() < 100;
        }
    }

    @Test
    public void testGenerationMinusExample() {
        for (int i = 0; i < 10000000; i+=1) {
            Example example = exampleGenerator.generateMinusExample();
            assert example.getOperation().equals("-");
            assert example.getFirstElement() >= 0;
            assert example.getFirstElement() <= 100;
            assert example.getSecondElement() >= 0;
            assert example.getSecondElement() < 100;
            assert example.getSecondElement() < example.getFirstElement();
            assert example.getResult() >= 0;
            assert example.getResult() <= 100;
        }
    }
}