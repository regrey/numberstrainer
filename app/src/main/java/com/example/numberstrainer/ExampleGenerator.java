package com.example.numberstrainer;

import java.util.Random;

public class ExampleGenerator {

    int NUMBER_OF_OPERATIONS = 2;

    private Random random;

    public ExampleGenerator() {
        random = new Random();
    }

    public Example getExample() {
        return generateOperation();
    }

    private Example generateOperation() {
        Example example;

        int operation = random.nextInt(NUMBER_OF_OPERATIONS);
        switch (operation) {
            case(0):
                example = generatePlusExample();
                break;
            case(1):
                example = generateMinusExample();
                break;
            default:
                example = null;
        }

        return example;
    }

    public Example generatePlusExample() {
        Example example = new Example();
        example.setOperation("+");
        int first_number = random.nextInt(100);
        example.setFirstElement(first_number);
        int second_number = random.nextInt(100 - first_number);
        example.setSecondElement(second_number);
        example.setResult(first_number + second_number);
        return example;
    }

    public Example generateMinusExample() {
        Example example = new Example();
        example.setOperation("-");
        int first_number = random.nextInt(100) + 1;
        example.setFirstElement(first_number);
        int second_number = random.nextInt(first_number);
        example.setSecondElement(second_number);
        example.setResult(first_number - second_number);
        return example;
    }

}
