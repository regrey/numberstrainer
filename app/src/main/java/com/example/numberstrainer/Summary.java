package com.example.numberstrainer;

public class Summary {

    private final int CASCADE = 30;
    private final int CASCADE_ATTEMPTS = 3;

    private int all;
    private int right;
    private int wrong;
    private int cascadeLine;
    private int cascadeAttempts;

    public Summary() {
        this.all = 0;
        this.right = 0;
        this.wrong = 0;
        this.cascadeLine = 0;
        this.cascadeAttempts = CASCADE_ATTEMPTS;
    }

    public void degreaseCascadeAttempts() {
        cascadeAttempts -= 1;
    }

    public void setCascadeAttempts(int cascadeAttempts) {
        this.cascadeAttempts = cascadeAttempts;
    }

    public void incrementAll() {
        all += 1;
    }

    public void incrementRight() {
        right += 1;
    }

    public void incrementWrong() {
        wrong += 1;
    }

    public void incrementCascadeLine() {
        cascadeLine += 1;
    }

    public void brokeCascadeLine() {
        cascadeLine = 0;
    }

    public void brokeCascadeAttempts() {
        cascadeAttempts = CASCADE_ATTEMPTS;
    }

    public int getCASCADE() {
        return CASCADE;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getWrong() {
        return wrong;
    }

    public void setWrong(int wrong) {
        this.wrong = wrong;
    }

    public int getCascadeLine() {
        return cascadeLine;
    }

    public void setCascadeLine(int cascadeLine) {
        this.cascadeLine = cascadeLine;
    }

    public int getCascadeAttempts() {
        return cascadeAttempts;
    }

    public int getCASCADE_ATTEMPTS() {
        return CASCADE_ATTEMPTS;
    }
}
