package com.example.numberstrainer;

public class Example {

    private String operation;
    private int firstElement;
    private int secondElement;
    private int result;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public int getFirstElement() {
        return firstElement;
    }

    public void setFirstElement(int firstElement) {
        this.firstElement = firstElement;
    }

    public int getSecondElement() {
        return secondElement;
    }

    public void setSecondElement(int secondElement) {
        this.secondElement = secondElement;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
