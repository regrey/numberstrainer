package com.example.numberstrainer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final int SUCCESS_DELAY =  500;
    private final int FALL_DELAY =  2500;
    private final int FALL_LONG_DELAY = FALL_DELAY * 2;

    private TextView exampleView;
    private Summary summary;
    private StringBuilder answerString;
    private Example example;
    private TextView summaryView;
    private TextView cascadeView;
    private ExampleGenerator exampleGenerator;
    private TextView answerView;
    private Handler handler;
    private TextView rightAnswerView;
    private Button btnAccept;
    private Set<Button> numberButtons;
    private Button btnBack;
    private ConstraintLayout congratulationField;
    private TextView congratulationSummaryInfo;
    private SharedPreferences sharedPreferences;
    private Button btnResetConfirm;
    private Button btnResetDecline;
    private ConstraintLayout resetField;
    private Button btnReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler(Looper.getMainLooper());
        summary = new Summary();

        summaryView = findViewById(R.id.summary_info);
        cascadeView = findViewById(R.id.success_line);

        exampleView = findViewById(R.id.example_field);
        rightAnswerView = findViewById(R.id.right_answer_field);

        exampleGenerator = new ExampleGenerator();
        example = new Example();

        answerString = new StringBuilder();

        answerView = findViewById(R.id.answer_field);
        answerView.setText(answerString);

        numberButtons = new HashSet<>();
        numberButtons.add(findViewById(R.id.key_1));
        numberButtons.add(findViewById(R.id.key_2));
        numberButtons.add(findViewById(R.id.key_0));
        numberButtons.add(findViewById(R.id.key_3));
        numberButtons.add(findViewById(R.id.key_4));
        numberButtons.add(findViewById(R.id.key_5));
        numberButtons.add(findViewById(R.id.key_6));
        numberButtons.add(findViewById(R.id.key_7));
        numberButtons.add(findViewById(R.id.key_8));
        numberButtons.add(findViewById(R.id.key_9));
        NumberClickListener numberClickListener = new NumberClickListener();
        for (Button button : numberButtons) {
            button.setOnClickListener(numberClickListener);
        }

        btnBack = findViewById(R.id.key_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answerString.length() > 0) {
                    answerString.deleteCharAt(answerString.length() - 1);
                    answerView.setText(answerString);
                }
            }
        });

        btnAccept = findViewById(R.id.kb_accept);
        btnAccept.setOnClickListener(this);

        btnReset = findViewById(R.id.button_reset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGameApproving();
            }
        });

        resetField = findViewById(R.id.reset_field);
        btnResetConfirm = findViewById(R.id.reset_yes_btn);
        btnResetConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
                returnToGame();
            }
        });
        btnResetDecline = findViewById(R.id.reset_no_btn);
        btnResetDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToGame();
            }
        });


        congratulationField = findViewById(R.id.congratulation_field);
        congratulationSummaryInfo = findViewById(R.id.congratulation_summary_info);

        sharedPreferences = getPreferences(MODE_PRIVATE);
        sharedPreferencesLoading();
        drawSummary();
        if (example.getOperation().equals(""))
            example = exampleGenerator.getExample();
        drawExample(example);
    }

    public class NumberClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (answerString.length() < 2) {
                answerString.append(((TextView) v).getText());
                answerView.setText((answerString));
            }
        }

    }
    public void sharedPreferencesSaving() {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("SUMMARY_ALL_ATTEMPTS", summary.getAll());
        editor.putInt("SUMMARY_RIGHT_ATTEMPTS", summary.getRight());
        editor.putInt("SUMMARY_WRONG_ATTEMPTS", summary.getWrong());
        editor.putInt("CASCADE_LINE_NUMBER", summary.getCascadeLine());
        editor.putInt("CASCADE_ATTEMPTS", summary.getCascadeAttempts());

        editor.putString("EXAMPLE_OPERATION", example.getOperation());
        editor.putInt("EXAMPLE_FIRST_ELEMENT", example.getFirstElement());
        editor.putInt("EXAMPLE_SECOND_ELEMENT", example.getSecondElement());
        editor.putInt("EXAMPLE_RESULT", example.getResult());

        editor.apply();
    }

    private void sharedPreferencesLoading() {
        if (sharedPreferences.contains("SUMMARY_ALL_ATTEMPTS")); summary.setAll(sharedPreferences.getInt("SUMMARY_ALL_ATTEMPTS", 0));
        if (sharedPreferences.contains("SUMMARY_RIGHT_ATTEMPTS")); summary.setRight(sharedPreferences.getInt("SUMMARY_RIGHT_ATTEMPTS", 0));
        if (sharedPreferences.contains("SUMMARY_WRONG_ATTEMPTS")); summary.setWrong(sharedPreferences.getInt("SUMMARY_WRONG_ATTEMPTS", 0));
        if (sharedPreferences.contains("CASCADE_LINE_NUMBER")); summary.setCascadeLine(sharedPreferences.getInt("CASCADE_LINE_NUMBER", 0));
        if (sharedPreferences.contains("CASCADE_ATTEMPTS")); summary.setCascadeAttempts(sharedPreferences.getInt("CASCADE_ATTEMPTS", 0));

        if (sharedPreferences.contains("EXAMPLE_OPERATION")); example.setOperation(sharedPreferences.getString("EXAMPLE_OPERATION", ""));
        if (sharedPreferences.contains("EXAMPLE_FIRST_ELEMENT")); example.setFirstElement(sharedPreferences.getInt("EXAMPLE_FIRST_ELEMENT", 0));
        if (sharedPreferences.contains("EXAMPLE_SECOND_ELEMENT")); example.setSecondElement(sharedPreferences.getInt("EXAMPLE_SECOND_ELEMENT", 0));
        if (sharedPreferences.contains("EXAMPLE_RESULT")); example.setResult(sharedPreferences.getInt("EXAMPLE_RESULT", 0));
    }

    private void drawExample(Example example) {
        StringBuilder exampleText = new StringBuilder();
        exampleText.append(example.getFirstElement()).append(" ").append(example.getOperation()).append(" ").append(example.getSecondElement()).append(" =");
        exampleView.setText(exampleText);
        rightAnswerView.setText(String.valueOf(example.getResult()));
    }

    private void drawSummary() {
        summaryView.setText("Всего: " + summary.getAll() + ", Правильных: " + summary.getRight() + ", Неправильных: " + summary.getWrong());
        cascadeView.setText(summary.getCascadeLine() + "/" + summary.getCASCADE() + " (" + summary.getCascadeAttempts() + ")");
    }

    @Override
    public void onClick(View v) {
        int answer;
        try {
            answer = Integer.valueOf(answerString.toString());
        } catch (NumberFormatException exception) {
            // FIXME if user didn't enter answer
            return;
        }
        summary.incrementAll();
        if (answer == example.getResult()) {
            rightAnswerManage();
        }
        else {
            wrongAnswerManage();
        }
        sharedPreferencesSaving();
    }

    private void rightAnswerManage() {
        summary.incrementRight();
        summary.incrementCascadeLine();
        answerView.setBackgroundColor(ContextCompat.getColor(this, R.color.right));
        drawSummary();
        setButtonsEnable(false);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                answerView.setBackgroundColor(Color.argb(0,0,0,0));
                answerString = new StringBuilder();
                answerView.setText(answerString);
                example = exampleGenerator.getExample();
                drawExample(example);
                setButtonsEnable(true);
            }
        }, SUCCESS_DELAY);
        if (summary.getCascadeLine() == summary.getCASCADE()) {
            finish_game();
        }
    }

    private void wrongAnswerManage() {
        summary.incrementWrong();
        answerView.setBackgroundColor(ContextCompat.getColor(this, R.color.wrong));
        summary.degreaseCascadeAttempts();
        if (summary.getCascadeAttempts() >= 0) {
            setButtonsEnable(false);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    drawSummary();
                    answerView.setBackgroundColor(Color.argb(0,0,0,0));
                    answerString = new StringBuilder();
                    answerView.setText(answerString);
                    setButtonsEnable(true);
                }
            }, FALL_DELAY);

        }
        else {
            setButtonsEnable(false);
            rightAnswerView.setBackgroundColor(ContextCompat.getColor(this, R.color.right));
            rightAnswerView.setTextColor(answerView.getCurrentTextColor());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    summary.brokeCascadeLine();
                    summary.brokeCascadeAttempts();
                    drawSummary();
                    answerView.setBackgroundColor(Color.argb(0,0,0,0));
                    answerString = new StringBuilder();
                    answerView.setText(answerString);
                    rightAnswerView.setBackgroundColor(Color.argb(0, 0, 0, 0));
                    rightAnswerView.setTextColor(Color.argb(0,0,0,0));
                    example = exampleGenerator.getExample();
                    drawExample(example);
                    setButtonsEnable(true);
                }
            }, FALL_LONG_DELAY);
        }
    }

    private void setButtonsEnable(boolean value) {
        btnAccept.setEnabled(value);
        btnBack.setEnabled(value);
        for (Button button : numberButtons) {
            button.setEnabled(value);
        }
    }

    private void finish_game() {
        setButtonsEnable(false);
        congratulationSummaryInfo.setText("Всего:" + summary.getAll() + " Правильно:" + summary.getRight() + " Не правильно:" + summary.getWrong() + " Неистраченных попыток:" + String.valueOf(summary.getCascadeAttempts()) + " из " + String.valueOf(summary.getCASCADE_ATTEMPTS()));
        congratulationField.setVisibility(View.VISIBLE);
    }

    private void resetGameApproving() {
        if (summary.getCascadeLine() == summary.getCASCADE()) {
            resetGame();
            returnToGame();
            return;
        }
        btnReset.setVisibility(View.INVISIBLE);
        resetField.setVisibility(View.VISIBLE);
    }

    private void resetGame() {
        summary = new Summary();
        drawSummary();
        example = exampleGenerator.getExample();
        drawExample(example);
        sharedPreferencesSaving();
    }

    private void returnToGame() {
        setButtonsEnable(true);
        congratulationField.setVisibility(View.INVISIBLE);
        resetField.setVisibility(View.INVISIBLE);
        btnReset.setVisibility(View.VISIBLE);
    }

}